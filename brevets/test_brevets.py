from datetime import datetime, timezone

from acp_times import open_time, close_time

import arrow

def test_distance_200():
    st = arrow.get(datetime(2019, 11, 12, 0, 0)).isoformat()
    # open time test
    assert open_time(120,200,st) == datetime(2019, 11, 12, 3, 32, tzinfo=timezone.utc).isoformat()
    assert open_time(175,200,st) == datetime(2019, 11, 12, 5, 9, tzinfo=timezone.utc).isoformat()
    assert open_time(205,200,st) == datetime(2019, 11, 12, 5, 53, tzinfo=timezone.utc).isoformat()
    # close time test
    assert close_time(120,200,st) == datetime(2019, 11, 12, 8, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(175,200,st) == datetime(2019, 11, 12, 11, 40, tzinfo=timezone.utc).isoformat()
    assert close_time(205,200,st) == datetime(2019, 11, 12, 13, 30, tzinfo=timezone.utc).isoformat()

def test_distance_300():
    st = arrow.get(datetime(2019, 11, 12, 0, 0)).isoformat()
    # open time test
    assert open_time(120,300,st) == datetime(2019, 11, 12, 3, 32, tzinfo=timezone.utc).isoformat()
    assert open_time(250,300,st) == datetime(2019, 11, 12, 7, 27, tzinfo=timezone.utc).isoformat()
    assert open_time(305,300,st) == datetime(2019, 11, 12, 9, 0, tzinfo=timezone.utc).isoformat()
    # close time test
    assert close_time(120,300,st) == datetime(2019, 11, 12, 8, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(250,300,st) == datetime(2019, 11, 12, 16, 40, tzinfo=timezone.utc).isoformat()
    assert close_time(305,300,st) == datetime(2019, 11, 12, 20, 0, tzinfo=timezone.utc).isoformat()

def test_distance_400():
    st = arrow.get(datetime(2019, 11, 12, 0, 0)).isoformat()
    # open time test
    assert open_time(120,400,st) == datetime(2019, 11, 12, 3, 32, tzinfo=timezone.utc).isoformat()
    assert open_time(250,400,st) == datetime(2019, 11, 12, 7, 27, tzinfo=timezone.utc).isoformat()
    assert open_time(405,400,st) == datetime(2019, 11, 12, 12, 8, tzinfo=timezone.utc).isoformat()
    # close time test
    assert close_time(120,400,st) == datetime(2019, 11, 12, 8, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(250,400,st) == datetime(2019, 11, 12, 16, 40, tzinfo=timezone.utc).isoformat()
    assert close_time(405,400,st) == datetime(2019, 11, 13, 3, 0, tzinfo=timezone.utc).isoformat()

def test_distance_600():
    st = arrow.get(datetime(2019, 11, 12, 0, 0)).isoformat()
    # open time test
    assert open_time(120,600,st) == datetime(2019, 11, 12, 3, 32, tzinfo=timezone.utc).isoformat()
    assert open_time(250,600,st) == datetime(2019, 11, 12, 7, 27, tzinfo=timezone.utc).isoformat()
    assert open_time(450,600,st) == datetime(2019, 11, 12, 13, 48, tzinfo=timezone.utc).isoformat()
    assert open_time(605,600,st) == datetime(2019, 11, 12, 18, 48, tzinfo=timezone.utc).isoformat()
    # close time test
    assert close_time(120,600,st) == datetime(2019, 11, 12, 8, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(250,600,st) == datetime(2019, 11, 12, 16, 40, tzinfo=timezone.utc).isoformat()
    assert close_time(450,600,st) == datetime(2019, 11, 13, 6, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(605,600,st) == datetime(2019, 11, 13, 16, 0, tzinfo=timezone.utc).isoformat()

def test_distance_1000():
    st = arrow.get(datetime(2019, 11, 12, 0, 0)).isoformat()
    # open time test
    assert open_time(120,1000,st) == datetime(2019, 11, 12, 3, 32, tzinfo=timezone.utc).isoformat()
    assert open_time(250,1000,st) == datetime(2019, 11, 12, 7, 27, tzinfo=timezone.utc).isoformat()
    assert open_time(450,1000,st) == datetime(2019, 11, 12, 13, 48, tzinfo=timezone.utc).isoformat()
    assert open_time(1005,1000,st) == datetime(2019, 11, 13, 9, 5, tzinfo=timezone.utc).isoformat()
    # close time test
    assert close_time(120,1000,st) == datetime(2019, 11, 12, 8, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(250,1000,st) == datetime(2019, 11, 12, 16, 40, tzinfo=timezone.utc).isoformat()
    assert close_time(450,1000,st) == datetime(2019, 11, 13, 6, 0, tzinfo=timezone.utc).isoformat()
    assert close_time(1005,1000,st) == datetime(2019, 11, 15, 3, 0, tzinfo=timezone.utc).isoformat()